#!/bin/bash

echo "Start installing conky ..."

sudo apt-get install conky -y

# copy the files to the userdirectory
cp -R ../copy/.conky ~/

# create symlink
ln -s ~/.conky/conkyrc ~/.conkyrc

# create startup file
cat <<EOT >> ~/.config/autostart/conky.desktop
[Desktop Entry]
Type=Application
Exec=conky
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name[en_US]=terminal
Name=terminal
Comment[en_US]=open conky
Comment=open conky
EOT

printf "... Done installing conky\n"
