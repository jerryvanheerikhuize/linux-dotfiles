#!/bin/bash

echo "Start installing nautilus-admin ..."

sudo apt-get install nautilus-admin -y
# restart nautilus
nautilus -q

printf "... Done installing nautilus admin\n"
