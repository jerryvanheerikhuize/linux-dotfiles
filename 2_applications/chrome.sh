#!/bin/bash

echo "Start installing chrome"

wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb

#remove the package
sudo rm google-chrome-stable_current_amd64.deb

printf "... Done installing chrome\n"
