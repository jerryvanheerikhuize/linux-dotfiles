#!/bin/bash

echo "Start installing audacity"

# NOTE: this repository triggers an error as described here
# https://bugs.launchpad.net/pinta/+bug/1607586
sudo add-apt-repository ppa:pinta-maintainers/pinta-stable -y
sudo apt-get install audacity -y

printf "... Done installing audacity\n"
