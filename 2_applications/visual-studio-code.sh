#!/bin/bash

echo "Start installing visual studio code"

wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" -y
sudo apt-get install code -y

printf "... Done installing visual studio code\n"
