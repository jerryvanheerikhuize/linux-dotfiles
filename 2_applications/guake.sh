#!/bin/bash

echo "Start installing guake ..."

sudo apt-get install guake -y

# create startup file
cat <<EOT >> ~/.config/autostart/guake.desktop
[Desktop Entry]
Type=Application
Exec=guake
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name[en_US]=terminal
Name=terminal
Comment[en_US]=open the guake terminal
Comment=open the guake terminal
EOT

printf "... Done installing guake\n"
