#!/bin/bash

echo "Start installing google-drive-ocamlfuse ..."

#sudo add-apt-repository ppa:alessandro-strada/ppa -y
#sudo apt-get install google-drive-ocamlfuse -y

# create folder
#mkdir ~/GoogleDrive

# create startup file
cat <<EOT >> ~/.config/autostart/google-drive-ocamlfuse.desktop
[Desktop Entry]
Type=Application
Exec=google-drive-ocamlfuse "/home/${USER}/GoogleDrive"
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name[en_US]=google drive
Name=google drive
Comment[en_US]=start google drive via ocamlfuse
Comment=start google drive via ocamlfuse
EOT

printf "... Done installing google-drive-ocamlfuse\n"
