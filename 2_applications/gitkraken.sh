#!/bin/bash

echo "Start installing gitkraken"

wget https://release.gitkraken.com/linux/gitkraken-amd64.deb
sudo dpkg -i gitkraken-amd64.deb

#remove the package
sudo rm gitkraken-amd64.deb

printf "... Done installing gitkraken\n"
