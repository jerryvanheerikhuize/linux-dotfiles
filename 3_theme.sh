#!/bin/bash

#################################################################################
# 
#	 ██░ ██  ▄▄▄       ██▓      █████▒    ▄▄▄▄    ██▀███   ▒█████    ██████      
#	▓██░ ██▒▒████▄    ▓██▒    ▓██   ▒    ▓█████▄ ▓██ ▒ ██▒▒██▒  ██▒▒██    ▒      
#	▒██▀▀██░▒██  ▀█▄  ▒██░    ▒████ ░    ▒██▒ ▄██▓██ ░▄█ ▒▒██░  ██▒░ ▓██▄        
#	░▓█ ░██ ░██▄▄▄▄██ ▒██░    ░▓█▒  ░    ▒██░█▀  ▒██▀▀█▄  ▒██   ██░  ▒   ██▒     
#	░▓█▒░██▓ ▓█   ▓██▒░██████▒░▒█░       ░▓█  ▀█▓░██▓ ▒██▒░ ████▓▒░▒██████▒▒ ██▓ 
#	 ▒ ░░▒░▒ ▒▒   ▓▒█░░ ▒░▓  ░ ▒ ░       ░▒▓███▀▒░ ▒▓ ░▒▓░░ ▒░▒░▒░ ▒ ▒▓▒ ▒ ░ ▒▓▒ 
#	 ▒ ░▒░ ░  ▒   ▒▒ ░░ ░ ▒  ░ ░         ▒░▒   ░   ░▒ ░ ▒░  ░ ▒ ▒░ ░ ░▒  ░ ░ ░▒  
#	 ░  ░░ ░  ░   ▒     ░ ░    ░ ░        ░    ░   ░░   ░ ░ ░ ░ ▒  ░  ░  ░   ░   
#	 ░  ░  ░      ░  ░    ░  ░            ░         ░         ░ ░        ░    ░  
#	                                           ░                              ░ 
#	-----------------------------------------------------------------------------
#
#	Developed by:			Jerry van Heerikhuize
#	Modified by:            Jerry van Heerikhuize
#
#	-----------------------------------------------------------------------------
#
#	Version:                1.0.0
#	Creation Date:          13/02/18
#	Modification Date:      13/02/18
#	Email:                  hello@halfbros.nl
#	Description:            Linux install script
#	File:                   3_theme.sh
#
#################################################################################


printf "Start setting up themes ...\n\n"

echo "Adding repositories..."
sudo add-apt-repository ppa:noobslab/themes -y
sudo add-apt-repository ppa:noobslab/icons2 -y
printf "Adding repositories... Done\n\n"

echo "Reading package lists..."
sudo apt-get update
printf "\n\n"

echo "Installing dependencies..."
sudo apt-get install arc-theme -y
sudo apt-get install uniform-icons -y
printf "Installing dependencies... Done\n\n"

echo "Configuring themes..."

printf "... Done setting up themes\n"