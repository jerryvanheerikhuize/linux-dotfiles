#!/bin/bash

#################################################################################
# 
#	 ██░ ██  ▄▄▄       ██▓      █████▒    ▄▄▄▄    ██▀███   ▒█████    ██████      
#	▓██░ ██▒▒████▄    ▓██▒    ▓██   ▒    ▓█████▄ ▓██ ▒ ██▒▒██▒  ██▒▒██    ▒      
#	▒██▀▀██░▒██  ▀█▄  ▒██░    ▒████ ░    ▒██▒ ▄██▓██ ░▄█ ▒▒██░  ██▒░ ▓██▄        
#	░▓█ ░██ ░██▄▄▄▄██ ▒██░    ░▓█▒  ░    ▒██░█▀  ▒██▀▀█▄  ▒██   ██░  ▒   ██▒     
#	░▓█▒░██▓ ▓█   ▓██▒░██████▒░▒█░       ░▓█  ▀█▓░██▓ ▒██▒░ ████▓▒░▒██████▒▒ ██▓ 
#	 ▒ ░░▒░▒ ▒▒   ▓▒█░░ ▒░▓  ░ ▒ ░       ░▒▓███▀▒░ ▒▓ ░▒▓░░ ▒░▒░▒░ ▒ ▒▓▒ ▒ ░ ▒▓▒ 
#	 ▒ ░▒░ ░  ▒   ▒▒ ░░ ░ ▒  ░ ░         ▒░▒   ░   ░▒ ░ ▒░  ░ ▒ ▒░ ░ ░▒  ░ ░ ░▒  
#	 ░  ░░ ░  ░   ▒     ░ ░    ░ ░        ░    ░   ░░   ░ ░ ░ ░ ▒  ░  ░  ░   ░   
#	 ░  ░  ░      ░  ░    ░  ░            ░         ░         ░ ░        ░    ░  
#	                                           ░                              ░ 
#	-----------------------------------------------------------------------------
#
#	Developed by:			Jerry van Heerikhuize
#	Modified by:            Jerry van Heerikhuize
#
#	-----------------------------------------------------------------------------
#
#	Version:                1.0.0
#	Creation Date:          10/02/18
#	Modification Date:      13/02/18
#	Email:                  hello@halfbros.nl
#	Description:            Linux install script
#	File:                   install.sh
#
#################################################################################

# add desktop image
# automatically set fonts
# set variables for user folder

clear

printf "Welcome to Halfbros. does Dotfiles.\nBefore u continue please make sure you configured the config.sh properly\n"
read -p "Press Y|y to start... " -n 1 -r

if [[ $REPLY =~ ^[Yy]$ ]]; then
    
    sleep 1
    clear

    . ./1_initialize.sh

    sleep 1
    clear
   
    printf "Start installing applications ...\n\n"
    . ./2_applications/audacity.sh
    . ./2_applications/chrome.sh
    . ./2_applications/conky.sh
    . ./2_applications/dconf-editor.sh    
    . ./2_applications/evolution.sh
    . ./2_applications/filezilla.sh
    . ./2_applications/git.sh
    . ./2_applications/gitkraken.sh
    . ./2_applications/gnome-tweak-tool.sh
    . ./2_applications/google-drive-ocamlfuse.sh
    . ./2_applications/guake.sh
    . ./2_applications/hardinfo.sh
    . ./2_applications/nautilus-admin.sh
    . ./2_applications/net-tools.sh
    . ./2_applications/pinta.sh
    . ./2_applications/spotify.sh
    . ./2_applications/visual-studio-code.sh
    printf "... Done installing applications\n"

    sleep 1
    clear

    . ./3_theme.sh

    sleep 1
    clear

    . ./4_config.sh

    sleep 1
    clear

    . ./5_postprocess.sh

    printf "Thank you for using Halfbros. does Dotfiles.\n"
    read -n 1 -s -r -p "Press any key to reboot"

    sudo reboot

fi
