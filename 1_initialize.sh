#!/bin/bash

#################################################################################
# 
#	 ██░ ██  ▄▄▄       ██▓      █████▒    ▄▄▄▄    ██▀███   ▒█████    ██████      
#	▓██░ ██▒▒████▄    ▓██▒    ▓██   ▒    ▓█████▄ ▓██ ▒ ██▒▒██▒  ██▒▒██    ▒      
#	▒██▀▀██░▒██  ▀█▄  ▒██░    ▒████ ░    ▒██▒ ▄██▓██ ░▄█ ▒▒██░  ██▒░ ▓██▄        
#	░▓█ ░██ ░██▄▄▄▄██ ▒██░    ░▓█▒  ░    ▒██░█▀  ▒██▀▀█▄  ▒██   ██░  ▒   ██▒     
#	░▓█▒░██▓ ▓█   ▓██▒░██████▒░▒█░       ░▓█  ▀█▓░██▓ ▒██▒░ ████▓▒░▒██████▒▒ ██▓ 
#	 ▒ ░░▒░▒ ▒▒   ▓▒█░░ ▒░▓  ░ ▒ ░       ░▒▓███▀▒░ ▒▓ ░▒▓░░ ▒░▒░▒░ ▒ ▒▓▒ ▒ ░ ▒▓▒ 
#	 ▒ ░▒░ ░  ▒   ▒▒ ░░ ░ ▒  ░ ░         ▒░▒   ░   ░▒ ░ ▒░  ░ ▒ ▒░ ░ ░▒  ░ ░ ░▒  
#	 ░  ░░ ░  ░   ▒     ░ ░    ░ ░        ░    ░   ░░   ░ ░ ░ ░ ▒  ░  ░  ░   ░   
#	 ░  ░  ░      ░  ░    ░  ░            ░         ░         ░ ░        ░    ░  
#	                                           ░                              ░ 
#	-----------------------------------------------------------------------------
#
#	Developed by:			Jerry van Heerikhuize
#	Modified by:            Jerry van Heerikhuize
#
#	-----------------------------------------------------------------------------
#
#	Version:                1.0.0
#	Creation Date:          12/02/18
#	Modification Date:      13/02/18
#	Email:                  hello@halfbros.nl
#	Description:            Linux install script
#	File:                   1_initialize.sh
#
#################################################################################


printf "Start initializing ...\n\n"


echo "Reading package lists..."
sudo apt-get update
printf "\n\n"

echo "Adding repositories..."
sudo add-apt-repository universe
printf "Adding repositories... Done\n\n"

echo "Installing dependencies..."
sudo apt-get install software-properties-common -y
sudo apt-get install apt-transport-https -y
sudo apt-get install wget -y
sudo apt-get install ubuntu-restricted-extras -y
printf "Installing dependencies... Done\n\n"

echo "Reading package lists..."
sudo apt-get update
printf "\n\n"


printf "... Done initializing\n"