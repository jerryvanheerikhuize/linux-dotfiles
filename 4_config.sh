#!/bin/bash

#################################################################################
# 
#	 ██░ ██  ▄▄▄       ██▓      █████▒    ▄▄▄▄    ██▀███   ▒█████    ██████      
#	▓██░ ██▒▒████▄    ▓██▒    ▓██   ▒    ▓█████▄ ▓██ ▒ ██▒▒██▒  ██▒▒██    ▒      
#	▒██▀▀██░▒██  ▀█▄  ▒██░    ▒████ ░    ▒██▒ ▄██▓██ ░▄█ ▒▒██░  ██▒░ ▓██▄        
#	░▓█ ░██ ░██▄▄▄▄██ ▒██░    ░▓█▒  ░    ▒██░█▀  ▒██▀▀█▄  ▒██   ██░  ▒   ██▒     
#	░▓█▒░██▓ ▓█   ▓██▒░██████▒░▒█░       ░▓█  ▀█▓░██▓ ▒██▒░ ████▓▒░▒██████▒▒ ██▓ 
#	 ▒ ░░▒░▒ ▒▒   ▓▒█░░ ▒░▓  ░ ▒ ░       ░▒▓███▀▒░ ▒▓ ░▒▓░░ ▒░▒░▒░ ▒ ▒▓▒ ▒ ░ ▒▓▒ 
#	 ▒ ░▒░ ░  ▒   ▒▒ ░░ ░ ▒  ░ ░         ▒░▒   ░   ░▒ ░ ▒░  ░ ▒ ▒░ ░ ░▒  ░ ░ ░▒  
#	 ░  ░░ ░  ░   ▒     ░ ░    ░ ░        ░    ░   ░░   ░ ░ ░ ░ ▒  ░  ░  ░   ░   
#	 ░  ░  ░      ░  ░    ░  ░            ░         ░         ░ ░        ░    ░  
#	                                           ░                              ░ 
#	-----------------------------------------------------------------------------
#
#	Developed by:			Jerry van Heerikhuize
#	Modified by:            Jerry van Heerikhuize
#
#	-----------------------------------------------------------------------------
#
#	Version:                1.0.0
#	Creation Date:          13/02/18
#	Modification Date:      13/02/18
#	Email:                  hello@halfbros.nl
#	Description:            Linux install script
#	File:                   3_theme.sh
#
#################################################################################


printf "Start configuration ...\n\n"

echo "Setting background..."
gsettings set org.gnome.desktop.background picture-options "none"
gsettings set org.gnome.desktop.background color-shading-type "vertical"
gsettings set org.gnome.desktop.background primary-color "#35D7E7"
gsettings set org.gnome.desktop.background secondary-color "#08E7A2"

gsettings set org.gnome.desktop.screensaver picture-options "none"
gsettings set org.gnome.desktop.screensaver color-shading-type "vertical"
gsettings set org.gnome.desktop.screensaver primary-color "#35D7E7"
gsettings set org.gnome.desktop.screensaver secondary-color "#08E7A2"
sleep 1
printf "Setting background... Done\n\n"

echo "Setting interface..."
gsettings set org.gnome.desktop.interface clock-show-seconds false
gsettings set org.gnome.desktop.interface clock-show-weekday true
gsettings set org.gnome.desktop.wm.preferences titlebar-font 'Noto Mono 11'
sleep 1
printf "Setting interface... Done\n\n"

echo "Setting themes..."
gsettings set org.gnome.desktop.interface gtk-theme "Arc-Dark"
gsettings set org.gnome.desktop.interface icon-theme "Uniform+ blue folder"
sleep 1
printf "Setting themes... Done\n\n"

echo "Setting peripherals..."
gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click false
gsettings set org.gnome.desktop.peripherals.touchpad natural-scroll false
gsettings set org.gnome.desktop.peripherals.mouse speed 0.32384341637010672
sleep 1
printf "Setting peripherals... Done\n\n"

echo "Setting nautilus..."
gsettings set org.gnome.nautilus.desktop home-icon-visible true
gsettings set org.gnome.nautilus.desktop trash-icon-visible true
gsettings set org.gnome.nautilus.desktop volumes-visible true

gsettings set org.gnome.nautilus.icon-view captions "['size', 'none', 'none']"
gsettings set org.gnome.nautilus.icon-view default-zoom-level 'small'

gsettings set org.gnome.nautilus.list-view default-visible-columns "['name', 'size', 'owner', 'permissions', 'date_modified']"
gsettings set org.gnome.nautilus.list-view default-zoom-level 'small'
gsettings set org.gnome.nautilus.list-view use-tree-view true

gsettings set org.gnome.nautilus.preferences show-hidden-files true
sleep 1
printf "Setting nautilus... Done\n\n"


echo "Setting dock..."
gsettings set org.gnome.shell.extensions.dash-to-dock autohide-in-fullscreen false
gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 34
gsettings set org.gnome.shell.extensions.dash-to-dock dock-position 'BOTTOM'
sleep 1
printf "Setting dock... Done\n\n"

printf "... Done configuration\n"
